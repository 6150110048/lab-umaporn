package com.example.basiclistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[]movie  = new String[]{"Batman vs Superman",
                "13 Hours",
                "Deadpool",
                "The Forest ",
                "The 5th Wave",
                "10 Cloverfield Lane",
                "Free State of Jones",
                "Triple 9",
                "Hail, Caesar!",
                "Captain America"};
        ListView listViewMovie = (ListView) findViewById(R.id.listViewMovie);
        ArrayAdapter adapter = new ArrayAdapter(this ,android.R.layout.simple_list_item_1,movie);
        listViewMovie.setAdapter(adapter);
    }

}