package com.example.myapp6150110048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        }
    public void Facebook(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/umaporn.chumjan/"));
        startActivity(intent);
    }
    public void Instragram(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/"));
        startActivity(intent);
    }

    public void onClickboiled(View view) {
        Button  boiled = (Button)findViewById(R.id.button1);
        Intent intent = new Intent(MainActivity.this,SecondActivity.class);
        startActivity(intent);
    }

    public void onClickpuff(View view) {
        Button  puff = (Button)findViewById(R.id.button2);
        Intent intent = new Intent(MainActivity.this,ThreeActivity.class);
        startActivity(intent);
    }

    public void onClickfried(View view) {
        Button  fried = (Button)findViewById(R.id.button3);
        Intent intent = new Intent(MainActivity.this,fourActivity.class);
        startActivity(intent);
    }


}