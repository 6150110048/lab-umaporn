package com.example.myapp6150110048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    public void onClickBack(View view) {
        Button Back = (Button)findViewById(R.id.button4);
        Intent intent = new Intent(SecondActivity.this ,MainActivity.class);
        startActivity(intent);
}
}