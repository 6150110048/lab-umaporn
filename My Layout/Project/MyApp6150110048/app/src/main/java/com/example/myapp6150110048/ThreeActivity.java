package com.example.myapp6150110048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ThreeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);
    }

    public void onClickBack(View view) {
        Button  Back = (Button) findViewById(R.id.button5);
        Intent intent = new Intent(ThreeActivity.this ,MainActivity.class);
        startActivity(intent);
    }

    public void onClickpuff(View view) {
    }
}